using UnityEngine;

public class BlockChild : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			CanvasManager.instance.winText = "Level failed!";
			CanvasManager.instance.win.color = Color.red;
			CanvasManager.instance.SwitchScene();
			Time.timeScale = 0;
		}
	}
}
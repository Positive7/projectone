using UnityEngine;

public class Destruct : BaseBehaviour
{
	[SerializeField, Range(0.0f, 500.0f)] private float force = 200.0f;
	[SerializeField, Range(0.0f, 5.0f)] private float radius = 1.0f;
	[SerializeField, Range(0.0f, 50.0f)] private float up = 1.0f;

	[SerializeField] private ForceMode forceMode;
	[SerializeField] private bool shouldDestroy;
	public GameObject player;

	private void OnEnable()
	{
		ResetPosition();
	}

	public void DestructThis()
	{
		collider.enabled = false;
		renderer.enabled = false;
		foreach (Rigidbody inChild in GetComponentsInChildren<Rigidbody>())
		{
			inChild.GetComponent<Destructible>().DestroyThis(force, localPosition, radius, up, forceMode, shouldDestroy);
			if (shouldDestroy) Destroy(gameObject, 1.0f);
		}

		Destroy(gameObject, 1.5f);
	}

	private void ResetPosition()
	{
		foreach (Destructible inChild in GetComponentsInChildren<Destructible>()) { StartCoroutine(inChild.ResetPosition()); }
	}

	private void Update()
	{
		if (player.transform.position.z - 2 > position.z) { Destroy(gameObject, 0.1f); }
	}
}
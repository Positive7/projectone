using UnityEngine;

public class Win : MonoBehaviour
{
	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			CanvasManager.instance.win.color = Color.green;
			CanvasManager.instance.winText = "Level finished!";
			CanvasManager.instance.FinishLine();
			Time.timeScale = 0;
		}
	}
}
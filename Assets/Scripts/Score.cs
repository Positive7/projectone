﻿using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
	[SerializeField] private TMP_Text scoreText;
	[SerializeField] private TMP_Text timeText;

	private void Update()
	{
		ScoreManager.instance.time += Time.deltaTime;
		scoreText.text = $"Score: {ScoreManager.instance.score}";
		timeText.text = ScoreManager.instance.time.ToString("n2");
	}
}
﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
	public static ScoreManager instance;
	public float score;
	public float time;

	private void Awake()
	{
		instance = this;
	}
}
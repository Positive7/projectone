using UnityEngine;

public class Block : MonoBehaviour
{
	public GameObject player;
	[SerializeField] public float y;

	private void OnEnable()
	{
		Transform tr = transform;
		float i = (10 - (int) tr.localScale.x) * .5f;
		Vector3 position = tr.position;
		position = new Vector3(Random.Range(-i, i), 0, position.z);
		transform.position = position;
	}

	private void Update()
	{
		if (transform.position.z < 10) Destroy(gameObject);
		if (player.transform.position.z > transform.position.z) { Destroy(gameObject, 0.1f); }
	}
}
﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class InputManager
{
	[Range(0.0f, 50.0f)] public float forwardSpeed = 5.0f;
	[Range(0.0f, 50.0f)] public float strafeSpeed = 2.0f;
	public KeyCode Sprint = KeyCode.LeftShift;
	public KeyCode Jump = KeyCode.Space;
	private float h => Input.GetAxis("Horizontal");

	public Vector3 velocity => new Vector3(h * strafeSpeed, 0, forwardSpeed);
}

public class PlayerController : BaseBehaviour
{
	public InputManager inputManager = new InputManager();

	[SerializeField, Range(0.0f, 50.0f)] private float jumpForce = 5.0f;
	[SerializeField, Range(0.0f, 50.0f)] private float fallForce = 5.0f;
	[SerializeField, Range(0.0f, 50.0f)] private float jumpHeight = 2.0f;
	[SerializeField] private Image slowDownUI;
	[SerializeField] private bool isGrounded, canJump;

	private IEnumerator recharge;
	private Vector3 targetVelocity;
	private float time = 0.2f;
	private float slowDown = 2.0f;
	private float speedTime;

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("Destructible")) { other.gameObject.GetComponent<Destruct>().DestructThis(); }
	}


	private void Update()
	{
		if(position.y < -5.0f)CanvasManager.instance.SwitchScene();
		inputManager.forwardSpeed = Mathf.SmoothStep(15, 30, speedTime / 60);
		speedTime += Time.deltaTime;
		isGrounded = position.y <= 0.51f;
		targetVelocity = inputManager.velocity;
		if (Input.GetKeyDown(inputManager.Jump) && isGrounded)
		{
			time = 0.2f;
			canJump = true;
		}

		slowDownUI.fillAmount = slowDown / 2.0f;
		if (Input.GetKey(inputManager.Sprint) && slowDown > 0.0f)
		{
			slowDown -= Time.deltaTime;
			targetVelocity /= 2.5f;
		}

		if (Input.GetKeyUp(inputManager.Sprint))
		{
			if (recharge != null) StopCoroutine(recharge);
			recharge = RechargeSlowdown(c => slowDown = c);
			StartCoroutine(recharge);
		}
	}


	private void FixedUpdate()
	{
		if (canJump)
		{
			rigidbody.AddForce(new Vector3(0, jumpHeight, 0) * jumpForce, ForceMode.Impulse);
			canJump = false;
		}

		if (!isGrounded)
		{
			time -= Time.deltaTime;
			if (time <= 0.0f) { rigidbody.AddForce(new Vector3(0, -jumpHeight, 0) * fallForce, ForceMode.Force); }
		}

		targetVelocity = transform.TransformDirection(targetVelocity);
		rigidbody.MovePosition(position + targetVelocity * Time.deltaTime);
	}


	private IEnumerator RechargeSlowdown(Action<float> slow)
	{
		yield return new WaitForSeconds(1.0f);
		slow(Mathf.Lerp(slowDown, 2.0f, Time.deltaTime));
		slow(2.0f);
		recharge = null;
	}
}
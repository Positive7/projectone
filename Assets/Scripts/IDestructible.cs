using System.Collections;
using UnityEngine;

public interface IDestructible
{
	void DestroyThis(float force, Vector3 pos, float radius, float upwardsModifier, ForceMode forceMode, bool shouldDestroy);
	IEnumerator ResetPosition();
}
using UnityEngine;

public class MainMenu : MonoBehaviour
{
	[SerializeField] private GameObject inGameMenu;

	private void Awake()
	{
		Time.timeScale = 0;
	}

	public void StartGame()
	{
		gameObject.SetActive(false);
		inGameMenu.SetActive(true);
		Time.timeScale = 1;
	}
}
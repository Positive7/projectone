﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileManager : MonoBehaviour
{
	[SerializeField] private GameObject Tile;
	[SerializeField] private GameObject player;
	[SerializeField] private GameObject[] ActiveTiles = new GameObject[10];
	[SerializeField] private GameObject destructible;
	[SerializeField] private GameObject blockPrefab;

	public float spawnZ;
	public static TileManager instance;

	private Queue<GameObject> Tiles = new Queue<GameObject>();

	private void Awake()
	{
		instance = this;
		Tiles = new Queue<GameObject>(10);
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));
		Tiles.Enqueue(Instantiate(Tile, transform));

		for (int i = 0; i < 10; i++)
		{
			ActiveTiles[i] = Tiles.Dequeue();
			ActiveTiles[i].transform.position = spawnZ * Vector3.forward;
		}
	}

	private void Update()
	{
		if (player != null && player.transform.position.z > spawnZ - 5 * 10)
		{
			for (int i = 0; i < 10; i++) { Tiles.Enqueue(ActiveTiles[i]); }

			GameObject tile = Tiles.Dequeue();
			tile.transform.position = spawnZ * Vector3.forward;
			if (spawnZ > 10.1f)
			{
				Vector3 position = tile.transform.position;
				StartCoroutine(Extensions.EaseInOut(x => tile.transform.position = x,
													new Vector3(position.x, 5, position.z),
													position, 2.0f));
			}

			spawnZ += 10.0f;
			Vector3 pos = tile.transform.position + new Vector3(Random.Range(-4, 4), 10.0f, 4.0f);
			GameObject destruct = Instantiate(destructible, pos, Quaternion.identity, transform);

			StartCoroutine(Extensions.EaseInOut(x =>
												{
													if (destruct != null) destruct.transform.localPosition = x;
												},
												pos,
												new Vector3(pos.x, 0.0f, pos.z), 2.0f));

			destruct.GetComponent<Destruct>().player = player;

			if (player.transform.position.z < 10) return;
			int rnd = Random.Range(0, 5);
			if (rnd != 3) return;
			GameObject block = Instantiate(blockPrefab, tile.transform.position + new Vector3(Random.Range(-2, 3), 0.0f, 0.0f), Quaternion.identity);

			block.transform.localScale = new Vector3(Random.Range(1, 11), Random.Range(1, 6), 1);
			block.AddComponent<Block>().player = player;
		}
	}
}

public static class Extensions
{
	public static IEnumerator EaseInOut(Action<Vector3> action, Vector3 min, Vector3 max, float time)
	{
		var t = 0.0f;
		while (t < 1.0f)
		{
			action(Ease(min, max, t));
			t += Time.deltaTime * time;
			yield return null;
		}

		action(max);
	}

	private static float Ease(float start, float end, float time)
	{
		time = Mathf.Clamp01(time);
		time = (Mathf.Sin(time * Mathf.PI * (0.2f + 2.5f * time * time * time)) * Mathf.Pow(1f - time, 2.2f) + time) * (1f + 1.2f * (1f - time));
		return start + (end - start) * time;
	}

	private static Vector3 Ease(Vector3 start, Vector3 end, float time)
	{
		return new Vector3(Ease(start.x, end.x, time), Ease(start.y, end.y, time), Ease(start.z, end.z, time));
	}
}
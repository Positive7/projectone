using UnityEngine;
using UnityEngine.EventSystems;

public class DefaultButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private GameObject isOver;
	public void OnPointerEnter(PointerEventData eventData) => isOver.SetActive(true);

	public void OnPointerExit(PointerEventData eventData) => isOver.SetActive(false);
}
﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Destructible : BaseBehaviour, IDestructible, IPhysical
{
	[SerializeField] private Vector3 startPosition;
	[SerializeField] private Quaternion startRotation;

	private void OnEnable()
	{
		rigidbody.useGravity = true;
		startPosition = localPosition;
		startRotation = localRotation;
	}

	public void DestroyThis(float force, Vector3 pos, float radius, float upwardsModifier, ForceMode forceMode, bool shouldDestroy)
	{
		renderer.enabled = true;
		EnablePhysics();
		rigidbody.useGravity = true;
		rigidbody.AddExplosionForce(force, pos, radius, upwardsModifier, forceMode);
		if (shouldDestroy) Destroy(gameObject, Random.Range(0.7f, 1.2f));
	}

	private void OnDestroy()
	{
		ScoreManager.instance.score += 1;
	}

	public IEnumerator ResetPosition()
	{
		DisablePhysics();
		yield return null;
		localPosition = startPosition;
		localRotation = startRotation;
		yield return null;
		rigidbody.velocity = Vector3.zero;
	}

	public void EnablePhysics()
	{
		rigidbody.isKinematic = false;
		rigidbody.useGravity = true;
	}

	public void DisablePhysics()
	{
		rigidbody.velocity = Vector3.zero;
		rigidbody.isKinematic = true;
		rigidbody.useGravity = false;
	}
}
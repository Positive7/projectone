using UnityEngine;
public interface IBase
{
	GameObject gameObject { get; }
	Transform transform { get; }
	Rigidbody rigidbody { get; }
	Collider collider { get; }
	Renderer renderer { get; }
}

public class BaseBehaviour : MonoBehaviour, IBase
{
	private GameObject m_gameObject;
	private Transform m_transform;
	private Rigidbody m_rigidbody;
	private Collider m_collider;
	private Renderer m_renderer;
	private Camera m_camera;

	public new Camera camera
	{
		get
		{
			if (m_camera == null) m_camera = Camera.main;
			return m_camera;
		}
	}

	public new GameObject gameObject
	{
		get
		{
			if (m_gameObject == null) m_gameObject = ((Component) this).gameObject;
			return m_gameObject;
		}
	}

	public new Transform transform
	{
		get
		{
			if (m_transform == null) m_transform = ((Component) this).transform;
			return m_transform;
		}
	}

	public new Rigidbody rigidbody
	{
		get
		{
			if (GetComponent<Rigidbody>() == null)
			{
				throw new MissingComponentException($"There is no Rigidbody component attached to the \"{name}\" -, but a script is trying to access it.");
			}

			if (m_rigidbody == null) m_rigidbody = GetComponent<Rigidbody>();
			return m_rigidbody;
		}
	}

	public new Collider collider
	{
		get
		{
			if (GetComponent<Collider>() == null)
			{
				throw new MissingComponentException($"There is no Collider component attached to the \"{name}\" -, but a script is trying to access it.");
			}

			if (m_collider == null) m_collider = GetComponent<Collider>();
			return m_collider;
		}
	}

	public new Renderer renderer
	{
		get
		{
			if (GetComponent<Renderer>() == null)
			{
				throw new MissingComponentException($"There is no Renderer component attached to the \"{name}\" -, but a script is trying to access it.");
			}

			if (m_renderer == null) m_renderer = GetComponent<Renderer>();
			return m_renderer;
		}
	}

	public Vector3 position
	{
		get => transform.position;
		set => transform.position = value;
	}

	public Vector3 localPosition
	{
		get => transform.localPosition;
		set => transform.localPosition = value;
	}

	public Quaternion rotation
	{
		get => transform.rotation;
		set => transform.rotation = value;
	}

	public Quaternion localRotation
	{
		get => transform.localRotation;
		set => transform.localRotation = value;
	}

	public Vector3 eulerAngles
	{
		get => transform.eulerAngles;
		set => transform.eulerAngles = value;
	}

	protected T GetOrAddComponent<T>() where T : Component
	{
		return gameObject.GetComponent<T>() == null ? gameObject.AddComponent<T>() : gameObject.GetComponent<T>();
	}
}
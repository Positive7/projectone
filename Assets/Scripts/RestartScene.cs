using TMPro;
using UnityEngine;

public class RestartScene : MonoBehaviour
{
	[SerializeField] private TMP_Text finalScore;
	[SerializeField] private TMP_Text timeText;


	private void OnEnable()
	{
		finalScore.text = $"Final score: {ScoreManager.instance.score}";
		timeText.text = $"Time: {Time.time:n2}\n Level: {CanvasManager.instance.lvl}";
		CanvasManager.instance.win.text = CanvasManager.instance.winText;
	}
}
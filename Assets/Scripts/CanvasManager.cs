using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
	[SerializeField] private GameObject restartScene, inGame, nextLevel, restart;
	public TMP_Text win;
	public static CanvasManager instance;
	public string winText;
	[SerializeField] private GameObject finishLine, player;

	public int lvl = 1;

	private void Awake()
	{
		instance = this;
	}

	public void SwitchScene()
	{
		restartScene.SetActive(true);
		restart.SetActive(true);
		inGame.SetActive(false);
		nextLevel.SetActive(false);
	}

	public void FinishLine()
	{
		restartScene.SetActive(true);
		restart.SetActive(false);
		inGame.SetActive(false);
		nextLevel.SetActive(true);
	}

	public void NewLevel()
	{
		restartScene.SetActive(false);
		inGame.SetActive(true);
		Time.timeScale = 1.0f;
		finishLine.transform.position += new Vector3(0.0f, 0.0f, 200.0f);
		TileManager.instance.spawnZ = 0;
		player.transform.position = new Vector3(0, 0.5f, 0);
		lvl++;
	}

	public void RestartScene()
	{
		SceneManager.LoadScene(0);
		Time.timeScale = 1.0f;
		ScoreManager.instance.time = 0.0f;
		lvl = 1;
	}
}